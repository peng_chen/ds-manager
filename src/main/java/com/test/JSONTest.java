package com.test;

import com.alibaba.fastjson.JSONObject;
import com.neo.util.Cache;
import org.junit.jupiter.api.Test;
import sun.misc.IOUtils;

import java.io.*;

public class JSONTest {



    @Test
    public void testMethod() {
        BufferedReader br = null;
        String path = "datasource.json";

        try {
            br = new BufferedReader(new InputStreamReader(JSONTest.class.getClassLoader().getResourceAsStream(path)));
            String line = br.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line + "\r\n");
                line = br.readLine();
            }

            JSONObject json = JSONObject.parseObject(sb.toString());
            System.out.println(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMethod1() {
        String path = "D:\\my\\ds-manager\\src\\main\\resources\\datasource.json";
       String result =  readJsonFile(path);
        System.out.printf(result);
    }

    @Test
    public void testMethod2() {
       JSONObject jsonObject =  Cache.getDataSource();
        System.out.printf(jsonObject.toJSONString());
    }

    /**
     * 读取json文件，返回json串
     * @param fileName
     * @return
     */
    public static String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            File jsonFile = new File(fileName);
            FileReader fileReader = new FileReader(jsonFile);

            Reader reader = new InputStreamReader(new FileInputStream(jsonFile),"utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
