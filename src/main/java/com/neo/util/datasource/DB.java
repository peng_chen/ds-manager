package com.neo.util.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.neo.util.Cache;
import com.neo.util.JDBCUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DB  {

    private static DB instance = new DB();

    private DB(){

    }

    public static DB getInstance(){
        return instance;
    }

     String urlName ;

    public String getUrlName() {
        return urlName;
    }

    public  void setUrlName(String urlName) {
        this.urlName = urlName;
    }



    private static Map<String, DataSource> urlNameMapperDs=new LinkedHashMap<>();

    static {
        //从缓存中拿到所有配置数据源数据
        Map<String,Object> dsMap  = Cache.getDataSourceMap();
        Map dbConfig = null;
        for (String urlName :dsMap.keySet() ) {
            dbConfig = (Map) dsMap.get(urlName);
            DruidDataSource dataSource = new DruidDataSource();
            dataSource.setDriverClassName(dbConfig.get("driverClassName")+"");//设置驱动
            dataSource.setUrl(dbConfig.get("url")+"");//设置url
            dataSource.setUsername(dbConfig.get("username")+"");//设置用户名
            dataSource.setPassword(dbConfig.get("password")+"");//设置密码
            urlNameMapperDs.put(urlName,dataSource);//将每个datasource放到对应的map中
        }
    }

    DataSource getDataSourceByUrlName(String urlName){
        return urlNameMapperDs.get(urlName);
    }



    public List<Map<String,Object>> query(String sql, Object... param) throws SQLException {
       DataSource ds =  getDataSourceByUrlName(urlName);
       Connection conn = null;
       List<Map<String, Object>> list = null;
        try{
            // 获得连接:
            conn = ds.getConnection();
            // 编写SQL：
            CommonJdbc jdbcUtil = new CommonJdbc(conn);
            list = jdbcUtil.excuteQuery(sql);
           // System.out.printf(list.toString());
        }catch(Exception e){
            throw e;
//            e.printStackTrace();
//            return new ArrayList<>();
        }
        return list;
    }











    public static void main(String[] args) throws SQLException {
        DB db = new DB();
        db.setUrlName("root_mysql");
        db.query(" select * from clazz;");
    }

    static Logger logger = LoggerFactory.getLogger(DB.class);

    static class Utils{

        public static void releaseResouce(ResultSet rst, PreparedStatement pst, Connection conn) {
            try {
                if(rst !=null && !rst.isClosed())rst.close();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            try {
                if(pst !=null && !pst.isClosed())pst.close();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            try {
                if(conn !=null && !conn.isClosed() )conn.close();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }


}
