package com.neo.util.datasource;

import com.neo.util.DateUtil;
import com.neo.util.JDBCUtil;
import com.neo.util.StringUtils;
import oracle.sql.BLOB;
import oracle.sql.CLOB;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CommonJdbc {
    /**
     * 日志对象
     */
    private Logger logger = Logger.getLogger(CommonJdbc.class);


    Connection conn ;

    private PreparedStatement pst = null;

    private Statement st = null;

    private ResultSet rst = null;


    /**
     * 构造方法
     *
     * @param connection
     * 			数据库连接
     */
    public CommonJdbc(Connection connection) {
        conn = connection ;
    }

    /**
     * SQL 查询将查询结果直接放入ResultSet中
     * @param sql SQL语句
     * @param params 参数数组，若没有参数则为null
     * @return 结果集
     */
    private ResultSet executeQueryRS(String sql, Object[]... params) throws SQLException {
        try {
            // 获得连接
            // 调用SQL
            pst = conn.prepareStatement(sql);
          // pst.setQueryTimeout(25000);//设置查询超时
            // 参数赋值
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    pst.setObject(i + 1, params[i]);
                }
            }
            // 执行
            rst = pst.executeQuery();
        } catch (SQLException e) {
           throw e;
        }/*finally {
            closeAll();
        }*/
        return rst;
    }

    public List<Map<String, Object>> excuteQuery(String sql, Object[]... params) throws SQLException {
        long start =System.currentTimeMillis();
        // 执行SQL获得结果集
        ResultSet rs = executeQueryRS(sql, params);
        // 创建ResultSetMetaData对象
        ResultSetMetaData rsmd = null;
        // 结果集列数
        int columnCount = 0;
        try {
            rsmd = rs.getMetaData();
            // 获得结果集列数
            columnCount = rsmd.getColumnCount();
        } catch (SQLException e1) {
           throw e1;
        }
        // 创建List
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            // 将ResultSet的结果保存到List中
            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                for (int i = 1; i <= columnCount; i++) {
                    if (rsmd.getColumnTypeName(i).equals("BLOB")){
                        BLOB v = (BLOB) rs.getBlob(i);
                        if (v==null){
                            map.put(rsmd.getColumnLabel(i),null);
                        }else{
                            map.put(rsmd.getColumnLabel(i), StringUtils.BlobToString(v));
                        }
                    }
                    if (rsmd.getColumnTypeName(i).equals("CLOB")){
                        CLOB v = (CLOB) rs.getClob(i);
                        if (v==null){
                            map.put(rsmd.getColumnLabel(i),"");
                        }else {
                            map.put(rsmd.getColumnLabel(i),StringUtils.ClobToString(v));
                        }

                    } else if(rsmd.getColumnTypeName(i).equals("DATE")){
                        Date v =  rs.getDate(i);
                        if (v==null){
                            map.put(rsmd.getColumnLabel(i),null);
                        }else {
                            map.put(rsmd.getColumnLabel(i), DateUtil.DateToStr(v));
                        }
                    }
                    else {
                        map.put(rsmd.getColumnLabel(i), rs.getObject(i));
                    }

                }
                list.add(map);//每一个map代表一条记录，把所有记录存在list中
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(sql);
        }  finally{
            closeAll();
        }
        long end =System.currentTimeMillis();
        logger.info("线程"+Thread.currentThread().getName()+"查询sql:"+sql+"花费时间"+((end-start)/1000)+"s");
        return list;
    }

    /**
     *  关闭数据连接
     *  /
     */
    private void closeAll(){

        try {
            if(rst !=null && !rst.isClosed())rst.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        try {
            if(pst !=null && !pst.isClosed())pst.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        try {
            if(st !=null && !st.isClosed())st.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        try {
            if(conn !=null && !conn.isClosed() )conn.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }


}
