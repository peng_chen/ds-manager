package com.neo.util.datasource.parse;

import java.util.Map;

public interface SqlParse {

     Map<String,String> parseSql(String sql);
}
