package com.neo.util.datasource.parse.mysql;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.mysql.parser.MySqlStatementParser;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlOutputVisitor;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.alibaba.druid.util.JdbcConstants;
import com.neo.util.datasource.parse.SqlParse;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MysqlParse implements SqlParse {

    @Override
    public Map<String, String> parseSql(String sql) {
        Map<String, String> result = new LinkedHashMap<>();
        result.put("status", "200");
        result.put("msg", "解析正常");
        try {

            MySqlStatementParser parser = new MySqlStatementParser(sql);
            List<SQLStatement> stmtList = parser.parseStatementList();

            // 将AST通过visitor输出
            StringBuilder out = new StringBuilder();
            MySqlOutputVisitor visitor = new MySqlOutputVisitor(out);

            for (SQLStatement stmt : stmtList) {
                stmt.accept(visitor);
                System.out.println(out + ";");
                out.setLength(0);
            }
        } catch (Exception e) {
            String msg = e.getMessage();
            String new_msg = msg.replace("ERROR", "错误").replace("token", "标记").replace("pos", "位置");
            result.put("status", "500");
            result.put("msg", new_msg);
        }
        return result;
    }


    public static void main(String[] args) {
        // String sql = "update t set name = 'x' where id < 100 limit 10";
        // String sql = "SELECT ID, NAME, AGE FROM USER WHERE ID = ? limit 2";
        // String sql = "select * from tablename limit 10";

        String sql = "select a.user from emp_table a left join b on a.id = b.id ";
        String dbType = JdbcConstants.MYSQL;

        //格式化输出
        String result = SQLUtils.format(sql, dbType);
        System.out.println(result); // 缺省大写格式
        List<SQLStatement> stmtList = SQLUtils.parseStatements(sql, dbType);

        //解析出的独立语句的个数
        System.out.println("size is:" + stmtList.size());
        for (int i = 0; i < stmtList.size(); i++) {

            SQLStatement stmt = stmtList.get(i);
            MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
            stmt.accept(visitor);

            //获取表名称
            System.out.println("Tables : " + visitor.getCurrentTable());
            //获取操作方法名称,依赖于表名称
            System.out.println("Manipulation : " + visitor.getTables());
            //获取字段名称
            System.out.println("fields : " + visitor.getColumns());
        }
    }
}
