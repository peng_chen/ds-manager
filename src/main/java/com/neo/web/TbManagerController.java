package com.neo.web;

import com.alibaba.fastjson.JSONObject;
import com.neo.service.TbManagerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/")
public class TbManagerController {

    private Logger logger = Logger.getLogger(TbManagerController.class);

    @Autowired
    TbManagerService tbManagerService;

    @CrossOrigin(origins = "*")
    @RequestMapping("/system/table/getTable")
    @ResponseBody
    public Map getTable(@RequestBody JSONObject object) throws Exception {
        return tbManagerService.getTable(object);
    }

}
