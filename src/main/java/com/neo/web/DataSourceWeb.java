package com.neo.web;

import com.neo.service.DataSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/")
@CrossOrigin(origins = "*")
public class DataSourceWeb {


    @Autowired
    DataSourceService dataSourceService;

    @RequestMapping("/getAllDataSource")
    @ResponseBody
    public Map<String, Object> getAllDataSource() {
        return dataSourceService.getAllDataSource();
    }


    @RequestMapping("/addDatasource")
    @ResponseBody
    public Map<String, Object> addDatasource(@RequestBody Map<String,Object> data) {
        return dataSourceService.addDatasource(data);
    }

    @RequestMapping("/updateDatasource")
    @ResponseBody
    public Map<String, Object> updateDatasource(@RequestBody Map<String,Object> data) {
        return dataSourceService.updateDatasource(data);
    }

    @RequestMapping("/getDataSourceByName")
    @ResponseBody
    public Map<String, Object> getDataSourceByName(@RequestBody Map<String,Object> data) {
        return dataSourceService.getDataSourceByName(data);
    }

    @RequestMapping("/getDbByUrlName")
    @ResponseBody
    public Map<String, Object> getDbByUrlName(@RequestBody Map<String,Object> data) {
        return dataSourceService.getDbByUrlName(data);
    }


}
