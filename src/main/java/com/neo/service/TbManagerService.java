package com.neo.service;

import com.alibaba.druid.sql.ast.SQLObject;
import com.alibaba.fastjson.JSONObject;
import com.neo.util.Cache;
import com.neo.util.StringUtils;
import com.neo.util.datasource.DB;
import com.neo.util.datasource.parse.SqlParse;
import com.neo.util.datasource.parse.mysql.MysqlParse;
import com.neo.web.TbManagerController;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.*;

@Service
public class TbManagerService {

    private Logger logger = Logger.getLogger(TbManagerService.class);


    public Map getTable(JSONObject object) throws Exception {
    try{

        String db_key = object.getString("url_name");
        String url = object.getString("url");
        //String dbName = getDbByUrl(url);
        String dbType =object.getString("dbType");
        String sql = (String) object.get("sql");//获得传入sql
        validate(sql);//校验sql
        DB db = DB.getInstance();//获取DB操作实例
        logger.info(db);
        db.setUrlName(db_key);

        Map mapper =  Cache.getUrlNameMapperDriver();

        List<Map<String, Object>> list =  db.query(sql);
        Map<String,Object> resultMap = new LinkedHashMap<>();

        String tb ="dual";
        String dbt ="test";
        List<Map<String, Object>> columns = getMysqlTbStruct( list);

        /*List<Map<String, Object>> column = db.query(sql);
        for (Map<String, Object>  m : column) {
            Map<String,Object> temp = new LinkedHashMap<>();
            temp.put("title",m.get("COLUMN_NAME"));
            temp.put("key",m.get("COLUMN_NAME"));
            columns.add(temp);
        }*/
        resultMap.put("columns",columns);
        Map<String,Object> data = new LinkedHashMap<>();
        data.put("records",list);
        resultMap.put("data",data);
        return resultMap;
    }
    catch (Exception e){
        Map<String,Object> resultMap = new LinkedHashMap<>();
        Map<String,Object> data = new LinkedHashMap<>();
        data.put("records",new ArrayList<>());
        resultMap.put("data",data);
        resultMap.put("status","500");
        resultMap.put("msg",e.getMessage());
        return resultMap;
     }
    }

    private List<Map<String,Object>> getMysqlTbStruct(List<Map<String,Object>> list) {
        if (list.size()==0) return new ArrayList<>();
        Map<String,Object> map = list.get(0);
        List<Map<String,Object>> result = new LinkedList<>();
        Map<String,Object> temp =null;
        for (String key:map.keySet() ) {
            temp = new LinkedHashMap<>();
            temp.put("title",key);
            temp.put("key",key);
            temp.put("minWidth",160);
            result.add(temp);
        }
        return result;
    }

    private List<Map<String, Object>> getMysqlTbStruct(String dbt, String tb) throws SQLException {
        DB db = DB.getInstance();//获取DB操作实例
        logger.info(db);
        String db_key ="root_mysql";
        db.setUrlName(db_key);
        String   sql ="SELECT   COLUMN_NAME  FROM    information_schema.`COLUMNS`\n" +
                "WHERE  TABLE_SCHEMA = '"+dbt+"' and TABLE_NAME='"+tb+"'" +
                "ORDER BY  TABLE_NAME,  ORDINAL_POSITION; ";
        return db.query(sql);
    }

    private void validate(String sql) throws Exception {
        boolean flag = true ;
        if (StringUtils.isBlank(sql)) flag = false;
        if (!flag){
            throw  new Exception("sql格式错误");
        }
        SqlParse sqlParse = new MysqlParse();
        Map<String,String> rs = sqlParse.parseSql(sql);
       if ("500".equals(rs.get("status")+"")){
           throw new Exception(rs.get("msg"));
       }
    }

    String getDbByUrl(String url){
        String str1=url.substring(0, url.indexOf("?"));//截取@之前的字符串
        String str2 = str1.substring(str1.lastIndexOf("/")+1);
        return str2;
    }

    String geTbByUrl(String url){
        String str2 = url.substring(url.lastIndexOf("from")+1);
        return str2;
    }


    public static void main(String[] args) {
        String str1="select * from t_hd_contract";//
        String image = str1.substring(str1.lastIndexOf("from")+1);
        System.out.println(image);
    }

}
