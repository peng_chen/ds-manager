/*
package com.neo;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayHandler;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.KeyedHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Test;

import com.mchange.v2.c3p0.ComboPooledDataSource;
public class JDBCUtilsTest {

    */
/**
     *   QueryRunner：核心执行类
     *   ResultSetHandler：提供对查询结果封装
     *   DbUtils    ：工具类
     *//*


    //c3p0 连接池
    public DataSource comboPooledDataSource = new ComboPooledDataSource();

    public Connection getConn() throws SQLException{

        return comboPooledDataSource.getConnection();
    }

    //插入数据
    public void saveData() throws SQLException {

        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);

        String sql = "insert into user values(null,'ruirui',?,?);";

        queryRunner.update(sql, "shenyang",27);
    }

    // 修改表

    public void fixData() throws SQLException{

        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql = "update user set name=? where id=?;";
        queryRunner.update(sql,"pp", 13);
    }


    // 删除操作
    public void demo3() throws SQLException{
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql ="delete from account where id =?";
        queryRunner.update(sql, 4);
    }


    // ArrayHandler:将查询到的一条记录封装到数组当中
    public void demo1() throws SQLException{
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql  = "select * from account where id = ?";
        Object[] objs = queryRunner.query(sql, new ArrayHandler(), 1); // ”1“ 代表一条数据
        System.out.println(Arrays.toString(objs));
    }

    //ArrayListHandler
    //一条查询是ArrayHandler 一个数组
    //多条查询 就是将多个数组 存入集合中
    public void demo2() throws SQLException {
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql  = "select * from user";

        List<Object[]> query = queryRunner.query(sql, new ArrayListHandler());

        for (Object[] objects : query) {

            for (Object object : objects) {
                System.out.println(object.toString());
            }
        }
    }

    //查询一条记录 返回的是一个bean对象
    public void demo4() throws SQLException {
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql  = "select * from user where id = ?";
        Man man = queryRunner.query(sql, new BeanHandler<>(Man.class),5);
        System.out.println(man.toString());
    }

    //注意返回的是一个标准的javabean对象，所在定义bean对象时候成员变量必须用private定义

    //查询一条记录 返回的是一个bean对象
    public void demo5() throws SQLException {
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql  = "select * from user";
        List<Man> query = queryRunner.query(sql, new BeanListHandler<>(Man.class ));
        System.out.println(query.toString());
    }

    // MapHandler:封装一条记录到Map中
    public void demo6()throws SQLException{
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql = "select * from user where id = ?";
        Map<String,Object> map = queryRunner.query(sql, new MapHandler() ,2);
        System.out.println(map);
    }

    // MapListHandler: //查询多条 将map集合存入list 集合中
    public void demo7()throws SQLException{
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql = "select * from user";
        List<Map<String,Object>> list = queryRunner.query(sql, new MapListHandler());
        for (Map<String, Object> map : list) {
            System.out.println(map);
        }
    }

    // ColumnListHandler ,返回的是一个列值的集合
    public void demo8()throws SQLException{
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql = "select id from user";
        Object list1  = queryRunner.query(sql, new ColumnListHandler());
        if (list1 instanceof  List){
            List list = (List)list1;
            for (Object object : list) {
                System.out.println(object.toString());
            }
        }

    }

    //ScalarHandler：单值查询
    public void demo9()throws SQLException{
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql = "select sum(age) from user;";
        Long count = (Long)queryRunner.query(sql, new ScalarHandler());
        System.out.println(count);
    }
    @Test
    // KeyedHandler:
    public void demo10()throws SQLException{
        QueryRunner queryRunner = new QueryRunner(comboPooledDataSource);
        String sql = "select * from user";
        Object obj= queryRunner.query(sql, new KeyedHandler("name"));
        Map map = new HashMap();
        if (obj instanceof  Map)
             map = (Map) obj;

        for (Object key : map.keySet()) {
            System.out.println(key + "   "+map.get(key));
        }
    }


    class Man{
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        String name ;


    }

}
*/
