package com.neo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mysql.jdbc.log.LogFactory;
import org.junit.Test;

import com.alibaba.druid.pool.DruidDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.rmi.runtime.Log;

public class DruidDemo {
    static Logger logger = LoggerFactory.getLogger(DruidDemo.class);

    @Test
    public void druidTest(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/test");
        dataSource.setUsername("root");
        dataSource.setPassword("123");
        try{
            // 获得连接:
            conn = dataSource.getConnection();
            // 编写SQL：
            String sql = "select * from user";
            pstmt = conn.prepareStatement(sql);
            // 执行sql:
            rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println(rs.getInt("id")+"   "+rs.getString("name"));
            }
        }catch(Exception e){
            e.printStackTrace();

            Utils.releaseResouce(rs, pstmt, conn);
        }

    }

    @Test
    public void druidTest1(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://10.255.1.171:3301/test?useUnicode=true&characterEncoding=UTF-8");
        dataSource.setUsername("root");
        dataSource.setPassword("EFAHhslaQrNvXBcM");
        try{
            // 获得连接:
            conn = dataSource.getConnection();
            // 编写SQL：
            String sql = "select * from clazz";
            pstmt = conn.prepareStatement(sql);
            // 执行sql:
            rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println(rs.getInt("id")+"   "+rs.getString("name"));
            }
        }catch(Exception e){
            e.printStackTrace();

            Utils.releaseResouce(rs, pstmt, conn);
        }

    }



    @Test
    public void druidTestOracle(){
        long start = System.currentTimeMillis();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        dataSource.setUrl("jdbc:oracle:thin:@10.255.2.220:1521:orcl");
        dataSource.setUsername("cmcdb");
        dataSource.setPassword("cmcdb");
        try{
            // 获得连接:
            conn = dataSource.getConnection();
            // 编写SQL：
            String sql = "select * from t_hd_contract";
            pstmt = conn.prepareStatement(sql);
            // 执行sql:
            rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println(rs.getInt("BANK_CODE")+"   "+rs.getString("BANK_CODE"));
            }
        }catch(Exception e){
            e.printStackTrace();

            Utils.releaseResouce(rs, pstmt, conn);
        }
        long end = System.currentTimeMillis();
        logger.info("记录 需要时间:"+(end - start)/1000+"s"); //批量插入需要时间:

    }


    @Test
    public void druidTestOracle1(){
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        dataSource.setUrl("oracle:thin:@//10.255.2.220:1521:orcl");
        dataSource.setUsername("accessuser");
        dataSource.setPassword("accessuser");
        try{
            // 获得连接:
            conn = dataSource.getConnection();
            // 编写SQL：
            String sql = "select 1 from dual";
            pstmt = conn.prepareStatement(sql);
            // 执行sql:
            rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println(rs.getInt("1"));
            }
        }catch(Exception e){
            e.printStackTrace();

            Utils.releaseResouce(rs, pstmt, conn);
        }

    }






    static class Utils{

        public static void releaseResouce(ResultSet rst, PreparedStatement pst, Connection conn) {
            try {
                if(rst !=null && !rst.isClosed())rst.close();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            try {
                if(pst !=null && !pst.isClosed())pst.close();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            try {
                if(conn !=null && !conn.isClosed() )conn.close();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }


}
