package com.neo;

import com.alibaba.druid.pool.DruidDataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class HikariCPTest {


    @Test
    public void druidTestMysql(){
        HikariConfig config = new HikariConfig();
        config.setMaximumPoolSize(100);
        config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        config.addDataSourceProperty("serverName", "localhost");
        config.addDataSourceProperty("port", "3306");
        config.addDataSourceProperty("databaseName", "test");
        config.addDataSourceProperty("user", "root");
        config.addDataSourceProperty("password", "123");
        HikariDataSource ds = new HikariDataSource(config);
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            // 获得连接:
            conn = ds.getConnection();
            // 编写SQL：
            String sql = "select * from user";
            pstmt = conn.prepareStatement(sql);
            // 执行sql:
            rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println(rs.getInt("id")+"   "+rs.getString("name"));
            }
        }catch(Exception e){
            e.printStackTrace();
            DruidDemo.Utils.releaseResouce(rs, pstmt, conn);
        }

    }


    @Test
    public void druidTestOracle(){
        //配置文件
        HikariConfig hikariConfig = new HikariConfig();
//        hikariConfig.setJdbcUrl("jdbc:mysql://localhost:3306/mydata");//mysql
        hikariConfig.setJdbcUrl("jdbc:oracle:thin:@localhost:1521/orcl.168.3.5");//oracle
        hikariConfig.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        hikariConfig.setUsername("hdt");
        hikariConfig.setPassword("hdt");
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        HikariDataSource ds = new HikariDataSource(hikariConfig);
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try{
            // 获得连接:
            conn = ds.getConnection();
            // 编写SQL：
            String sql = "select * from t_hd_contract";
            pstmt = conn.prepareStatement(sql);
            // 执行sql:
            rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println(rs.getInt("BANK_CODE")+"   "+rs.getString("HD_CONT_UUID"));
            }
        }catch(Exception e){
            e.printStackTrace();
            DruidDemo.Utils.releaseResouce(rs, pstmt, conn);
        }

    }

    @Test
    public void druidTestRemoteOracle(){
        //配置文件
        HikariConfig hikariConfig = new HikariConfig();
//        hikariConfig.setJdbcUrl("jdbc:mysql://localhost:3306/mydata");//mysql
        hikariConfig.setJdbcUrl("jdbc:oracle:thin:@10.255.2.220:1521:orcl");//oracle
        hikariConfig.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        hikariConfig.setUsername("cmcdb");
        hikariConfig.setPassword("cmcdb");
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        HikariDataSource ds = new HikariDataSource(hikariConfig);
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try{
            // 获得连接:
            conn = ds.getConnection();
            // 编写SQL：
            String sql = "select * from t_hd_contract";
            pstmt = conn.prepareStatement(sql);
            // 执行sql:
            rs = pstmt.executeQuery();
            while(rs.next()){
                System.out.println(rs.getInt("BANK_CODE")+"   "+rs.getString("BANK_CODE"));
            }
        }catch(Exception e){
            e.printStackTrace();
            DruidDemo.Utils.releaseResouce(rs, pstmt, conn);
        }

    }

}
