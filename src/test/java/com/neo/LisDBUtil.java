/*
package com.neo;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.List;

public class LisDBUtil {
    private static LisDBUtil instance;

    //c3p0被称为数据库连接池，用来管理数据库的连接
    //c3p0连接池的ComboPooledDataSource类
    private  ComboPooledDataSource dataSource;
    private  LisDBUtil()throws Exception{
        dataSource = new ComboPooledDataSource();
        dataSource.setUser("eafbim");
        dataSource.setPassword("eafbim");
        dataSource.setPassword("oracle");
        dataSource.setJdbcUrl("jdbc:oracle:thin:@127.0.0.1:1521/orcl.168.3.5");
        //dataSource.setJdbcUrl("jdbc:oracle:thin:@127.0.0.1:1521:system");
        dataSource.setDriverClass("oracle.jdbc.driver.OracleDriver");
        //dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/pjxt?useUnicode=true&characterEncoding=utf-8");
        //dataSource.setDriverClass("com.mysql.jdbc.Driver");
        dataSource.setMinPoolSize(5);//设置连接池的最小连接数
        dataSource.setMaxPoolSize(50);//设置连接池的最大连接数
        dataSource.setInitialPoolSize(10);//初始化时获取的连接数，取值应在minPoolSize与maxPoolSize之间。Default: 3
        dataSource.setMaxIdleTime(10);//最大空闲时间,10秒内未使用则连接被丢弃。若为0则永不丢弃。Default: 0
        dataSource.setAcquireIncrement(5);//当连接池中的连接耗尽的时候c3p0一次同时获取的连接数。Default: 3
        dataSource.setCheckoutTimeout(10000);//连接池用完时客户调用getConnection()后等待获取连接的时间，单位：毫秒
        dataSource.setBreakAfterAcquireFailure(false);//为true会导致连接池占满后不提供服务。所以必须为false
        dataSource.setIdleConnectionTestPeriod(30);//每30秒检查一次空闲连接，加快释放连接。
    }

    public static final LisDBUtil getInstance () {

        if (instance == null) {
            try {
                instance = new LisDBUtil();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;

    }
    //返回一个连接
    public synchronized final Connection getConnection () {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);//设置自动提交为否
            System.out.println("连接成功！");
            return conn;

        } catch (Exception e) {
            e.printStackTrace();

        }

        return null;

    }
    */
/**
 * 提交数据
 * @throws Exception
 *//*

    public  static void commit(Connection conn) throws Exception {
        if(null!=conn){
            conn.commit();//提交
        }
        if (null!=conn){
            conn.close();//关闭，其实连接并未真正的关闭，而是放回连接池中
        }
    }

    */
/**
 * 回滚
 * @throws Exception
 *//*

    public static void rollback( Connection conn) throws Exception {
        if (null!=conn){
            conn.rollback();//LIS数据回滚
        }
        if (null!=conn){
            conn.close();

        }
    }
    public static  void connColse(Connection conn){
        if(null!=conn){
            try{
                conn.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    public static void main(String[] args) throws IOException {
      */
/*  LisDBUtil lisDBUtil = LisDBUtil.getInstance();
        Connection conn = lisDBUtil.getConnection();
        System.out.println();*//*

        // 根据 mybatis-config.xml 配置的信息得到 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 然后根据 sqlSessionFactory 得到 session
        SqlSession session = sqlSessionFactory.openSession();
        // 最后通过 session 的 selectList() 方法调用 sql 语句 listStudent
        List<Student> listStudent = session.selectList("listStudent");
        for (Student student : listStudent) {
            System.out.println("ID:" + student.getId() + ",NAME:" + student.getName());
        }
    }


    class Student{
        String id ;
        String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
*/
